package com.eightam.android.actionbartogglebadge;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;

public class BadgeDrawerArrowDrawable extends DrawerArrowDrawable {

    private Paint paint;
    private boolean shouldShowBadge;

    public BadgeDrawerArrowDrawable(Context context) {
        super(context);
        initialize();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (shouldShowBadge) {
            drawBadge(canvas);
        }
    }

    public boolean shouldShowBadge() {
        return shouldShowBadge;
    }

    public void setShouldShowBadge(boolean shouldShowBadge) {
        this.shouldShowBadge = shouldShowBadge;
    }

    private void initialize() {
        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);

        shouldShowBadge = false;
    }

    private void drawBadge(Canvas canvas) {
        int x = (int) (0.75f * getBounds().width());
        int y = (int) (0.25f * getBounds().height());
        int r = (int) (0.25f * getBounds().width());

        float scale = 1.0f - getProgress();

        canvas.save();
        canvas.scale(scale, scale, x, y);;
        canvas.drawCircle(x, y, r, paint);
        canvas.restore();
    }

}
