package com.eightam.android.actionbartogglebadge;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.drawer)
    protected DrawerLayout drawer;

    private ActionBarDrawerToggle drawerToggle;
    private DrawerArrowDrawable drawerArrowDrawable;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) && super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @OnClick(R.id.buttonToggleBadge)
    protected void onToggleBadge() {
        BadgeDrawerArrowDrawable drawable = (BadgeDrawerArrowDrawable) drawerArrowDrawable;
        drawable.setShouldShowBadge(!drawable.shouldShowBadge());
        drawable.invalidateSelf();
    }

    private void initializeViews() {
        ButterKnife.bind(this);
        initializeToolbarAndDrawerLayout();
    }

    private void initializeToolbarAndDrawerLayout() {
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, 0, 0);
        drawerArrowDrawable = new BadgeDrawerArrowDrawable(this);

        drawerToggle.setDrawerArrowDrawable(drawerArrowDrawable);
        drawer.addDrawerListener(drawerToggle);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

}
